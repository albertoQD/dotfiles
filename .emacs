(menu-bar-mode -1)
(toggle-scroll-bar -1)
(tool-bar-mode -1)

(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(require 'use-package)

(use-package flycheck
  :ensure t
  :init (global-flycheck-mode)
  :defer t)

(use-package blacken
  :ensure t
  :defer t
  :config
  (add-hook 'python-mode-hook 'blacken-mode))

(add-hook 'python-mode-hook
	  (lambda () (add-to-list 'write-file-functions 'delete-trailing-whitespace)))

(use-package mood-line
  :ensure t
  :init (mood-line-mode)
  :defer t)

(use-package avy
  :ensure t
  :defer t
  :bind (("C-:" . avy-goto-char-2)
	 ("C-'" . avy-goto-char)
	 ("C-;" . avy-goto-word-1)))

(use-package which-key
  :ensure t
  :config
  (progn
    (setq which-key-idle-delay 0.5)
    (setq which-key-idle-secondary-delay 0.05))
  (which-key-mode))

(use-package all-the-icons-dired
  :defer t
  :ensure t
  :hook (all-the-icons-dired-mode))

(use-package emojify
  :ensure t
  :hook (after-init . global-emojify-mode))

(use-package modus-themes
  :ensure t
  :defer t)

(use-package gruvbox-theme
  :ensure t
  :defer t)

(use-package gruber-darker-theme
  :ensure t
  :defer t)

(use-package general
  :ensure t
  :after which-key
  :config
  (general-override-mode 1)
  (general-create-definer tyrant-def
    :states '(normal visual motion emacs)
    :keymaps 'override
    :prefix "SPC"
    "SPC" 'helm-M-x
    "" nil

    ;; Quitting
    "q" '(:ignore t :which-key "exit")
    "qq" 'save-buffers-kill-terminal

    ;; Eval
    "e" '(:ignore t :which-key "eval")
    "eb" 'eval-buffer
    "er" 'eval-region
    "ee" 'eval-expression
    "el" 'eval-last-sexp

    ;; Themes
    "t" '(:ignore t :which-key "themes")
    "tt" 'load-theme
    "te" 'enable-theme
    "tD" 'disable-all-themes
    "tl" 'load-theme-light
    "td" 'load-theme-dark

    ;; Buffers
    "b" '(:ignore t :which-key "buffer")
    "bb" 'helm-buffers-list
    "bk" 'kill-this-buffer
    "bn" 'next-buffer
    "bp" 'previous-buffer
    "br" 'rename-buffer'
    "bR" 'revert-buffer'

    ;; Window
    "w" '(:ignore t :which-key "window")
    "wh" 'split-window-horizontally
    "wv" 'split-window-vertically
    "ww" 'other-window
    "wd" 'delete-window
    "wo" 'delete-other-windows

    ;; File
    "f" '(:ignore t :which-key "file")
    "ff" 'helm-find-files
    "fc" 'write-file
    "fc" 'dired-jump
    "fs" 'save-buffer
    "fe" 'find-user-init-file

    ;; Bookmarks
    "r" '(:ignore t :which-key "bookmarks")
    "rm" 'bookmark-set
    "rb" 'helm-filtered-bookmark
    "rl" 'bookmark-bmenu-list

    ;; Git
    "g" '(:ignore t :which-key "git")
    "gs" 'magit-status
    "gp" 'magit-push
    "gb" 'magit-branch
    "gB" 'magit-blame
    "gl" 'magit-log

    ;; Terminals
    "o" '(:ignore t :which-key "terminal")
    "oe" 'eshell
    "oa" 'ansi-term
    "ot" 'term
    "oo" 'shell-pop

    ;; Project
    "p" '(:ignore t :which-key "project")
    "pd" 'projectile-dired
    "pb" 'projectile-switch-to-buffer
    "pf" 'projectile-find-file
    "pg" 'helm-browse-project
    "ps" 'helm-projectile-grep
    "pp" 'helm-projectile-switch-project

    )

  (general-define-key
   :keymaps 'key-translation-map
   "ESC" (kbd "C-g")))

(defun disable-all-themes ()
  "Disable all themes."
  (interactive)
  (mapc 'disable-theme custom-enabled-themes))

(defun find-user-init-file ()
  "Find user init file."
  (interactive)
  (find-file user-init-file))

(defun load-theme-light ()
  "Load theme light."
  (interactive)
  (load-theme 'modus-operandi t))

(defun load-theme-dark ()
  "Load theme dark."
  (interactive)
  (load-theme 'gruber-darker t))

(use-package evil
  :ensure t
  :hook (after-init . evil-mode)
  :general (tyrant-def)
  :init
  (progn
    (setq evil-toggle-key "C-`")
    (setq evil-want-C-i-jump nil)))

(use-package helm
  :ensure t
  :bind (
  ("M-x" . helm-M-x)
   :map helm-map
	("<tab>" . helm-execute-persistent-action)
	("C-i" . helm-execute-persistent-action))
  :init
  :config
  (helm-mode))

(use-package helm-ls-git
  :ensure t)

(use-package helm-projectile
  :ensure t
  :config
  (helm-projectile-on))

(use-package projectile
  :ensure t
  :config
  (projectile-mode))

(use-package magit
  :ensure t)

(use-package shell-pop
  :ensure t
  ;; :bind
  ;; (("C-x t" . shell-pop))
  :config
  (progn
    (setq shell-pop-shell-type (quote ("eshell" "*eshell*" (lambda nil (eshell shell-pop-term-shell)))))
    (setq shell-pop-window-size 30)
    (setq shell-pop-window-position "bottom")
    (setq shell-pop-full-span t)
    (setq shell-pop-autocd-to-working-dir t)
    (setq shell-pop-restore-window-configuration t)
    (setq shell-pop-cleanup-buffer-at-process-exit t))
  ;; need to do this manually or not picked up by `shell-pop'
  (shell-pop--set-shell-type 'shell-pop-shell-type shell-pop-shell-type))

(use-package exec-path-from-shell
  :ensure t
  :if (memq window-system '(mac ns))
  :config
  (setq exec-path-from-shell-arguments '("-l"))
  (exec-path-from-shell-initialize)
  (exec-path-from-shell-copy-envs
   '("LC_ALL" "LANG" "LC_TYPE"
     "SSH_AGENT_PID" "SHELL"
     "ZSH" "PATH")))

(setenv "PATH" "/opt/homebrew/bin:/opt/homebrew/sbin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:/Users/aquintero/.cargo/bin")
(setq exec-path (split-string (getenv "PATH") path-separator))

(use-package markdown-mode
  :ensure t
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . markdown-mode)
         ("\\.markdown\\'" . markdown-mode))
  :init (setq markdown-command "multimarkdown"))

(use-package org-bullets
  :ensure t
  :commands (org-bullets-mode)
  :init (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1))))

(put 'dired-find-alternate-file 'disabled nil)
;;(setq mac-option-modifier 'meta)
(setq mac-command-modifier 'meta)
;;(setq mac-left-option-modifier 'meta)
(setq mac-right-option-modifier nil)
;; (setq mac-command-key-is-meta t)
(setq select-enable-clipboard t)
(setq org-hide-emphasis-markers t)
(setq org-todo-keywords
      '((sequence "TODO" "WAITING" "ONGOING" "|" "DONE" "ABANDONED")))
(setq backup-directory-alist `(("." . "~/.saves")))
(setq auto-save-file-name-transforms '((".*" "~/.saves" t)))

;; (global-display-line-numbers-mode)
;; (setq display-line-numbers-type 'relative)
(load-theme-dark)
(set-face-attribute 'default nil
                    :family "Menlo"
                    :height 140
                    :weight 'normal
                    :width 'normal)

(setq inhibit-startup-message t
      inhibit-startup-echo-area-message user-login-name
      inhibit-default-init t
      initial-major-mode 'fundamental-mode
      initial-scratch-message nil )

(setq custom-file "~/.custom.el")

;;; .emacs ends here
(put 'upcase-region 'disabled nil)
