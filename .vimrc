syntax on
set tabstop=2
set shiftwidth=2
set expandtab
set ai
set number
set relativenumber
set nohlsearch
set ruler
highlight Comment ctermfg=green
highlight LineNr ctermfg=grey
highlight CursorLineNr ctermfg=grey
