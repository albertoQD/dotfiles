#!/bin/sh

echo "Installing dotfiles"
echo "-------------------"

check_previous_file()
{
    echo "  >>  Checking previous version of $1"
    if [ -e $1 ];
    then
       if [ -L $1 ];
       then
          rm $1 
	  echo "  >>  Previous symbolic link $1 was deleted"
       else
          mv $1 $1"_old"
	  echo "  >>  Previous $1 was moved to $1_old"
       fi
    fi
}

install_file()
{
	read -p "> Do you want to install ${1} [Y/n] " yn;
	case $yn in
		[Yy]* ) check_previous_file $HOME/$1 && \
			ln -s $PWD/$1 $HOME/$1 && \
			echo "  >>  Symbolic link for ${1} was created";;
	esac
}

install_file .config/kitty
install_file .zshrc
install_file .tmux.conf
install_file .vimrc
install_file .emacs
install_file .xinitrc
install_file .Xresources
install_file .config/bspwm
install_file .config/sxhkd
install_file .config/polybar
install_file .config/rofi
install_file .config/i3
install_file .config/autostart

echo "> Done with installation"
