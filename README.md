## Software

  - polybar
  - sxhkd
  - bspwm
  - urxvt
  - ligthdm
  - emacs
  - rofi
  - i3-gaps (i3bar i3status);
  - firefox
  - pcmanfm
  - lxrandr
  - lxappearance
  - feh
  - openvpn
  - gnome-screenshot
  - zsh
  - zsh-syntax-hightlighting
  - zsh-autosuggestions

## Fedora

```sh
sudo dnf install polybar sxhkd bspwm urxvt emacs vim rofi pcmanfm lxrandr lxappearance feh openvpn gnome-screenshot zsh zsh-syntax-highlighting zsh-autosuggestions
```
